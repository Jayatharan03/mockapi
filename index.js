const express = require('express');
const cors = require('cors')
const PORT = 5000

const app = express()
app.use(express.json())

const { save, refresh, getAll, getById, deleteById, updateById, addNewItem, addNewCollection } = require('./operation.js')

app.use(cors())


app.get("/reload",(req,res)=>{
    refresh()
    res.send("Refreshed")    
})

app.get("/",(req,res)=>{
    res.send("Server is running")
})

app.post("/newCollection",(req,res)=>{
    var name = req.body.name
    res.send(addNewCollection(name))
})

app.get("/save",(req,res)=>{
    save()
    res.send("Repo Saved")
})

app.get('/:c_name',(req,res)=>{
    const c_name = req.params.c_name
    res.send(getAll(c_name))
})

app.get('/:c_name/:id',(req,res)=>{
    const c_name = req.params.c_name
    const id = req.params.id
    res.send(getById(c_name,id))
})

app.post('/:c_name',(req,res)=>{
    const c_name = req.params.c_name
    const data = req.body.data
    addNewItem(c_name,data)
    res.send(getAll(c_name))
})

app.delete('/:c_name/:id',(req,res)=>{
    const c_name = req.params.c_name
    const id = req.params.id
    deleteById(c_name,id)
    res.send("Deleted")
})

app.put('/:c_name/:id',(req,res)=>{
    const c_name = req.params.c_name
    const id = req.params.id
    const data = req.body.data
    updateById(c_name,id,data)
    res.send(getById(c_name,id))
})

app.listen(PORT,(err)=>{
    console.log(`server running on PORT=${PORT}`)
})