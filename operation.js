const fs = require('fs');

var repo = require('./repo.json')

const getId = ()=>{
    return Date.now()
}

const save = ()=>{
    let data = JSON.stringify(repo)
    fs.writeFile('repo.json', data, 'utf8', ()=>{});
}

const refresh = ()=>{
    repo = require('./repo.json')
}

const getAll = (item)=>{
    return repo[item]
}

const getById = (item,id)=>{
    var res = repo[item].filter(itm => itm.id == id)
    return res[0];
}

const deleteById = (item,id)=>{
    var res = repo[item].filter(itm => itm.id != id)
    repo[item] = res
    save()
}

const updateById = (item,id,data)=>{
    var res = repo[item].map((itm)=>{
        if(itm.id == id){
            var itm = {"id":id,...data}
        }
        return itm
    })
    repo[item] = res
    save()
}

const addNewItem = (item,data)=>{
    var itm = {"id":getId(),...data}
    repo[item].push(itm)
    save()
}

const addNewCollection = (item)=>{
    repo[item] = []
    save()
    return repo[item]
}


module.exports = {save, refresh, getAll, getById, deleteById, updateById, addNewItem, addNewCollection}